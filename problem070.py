import numpy as np
import common_funcs as cf

# https://de.wikipedia.org/wiki/Eulersche_Phi-Funktion
limit = 100000
primes = cf.find_primes_below(limit+1)
rate = [[0,0.0,0.0]] * (limit+1)
rate[0]=[-1,-1,-1]
rate[1]=[1,1,1]
prime_factor_cache = [False] * (limit + 1)
for p in primes:
    prime_factor_cache[p] = [p]
filled = 2
sorted_vals = [None for i in range(limit+1)]

val_buf = [0] + [1-(1/v) for v in range(1,limit + 1)]

print("Limit",limit)
print(primes[:3])
print("calculated prime sieve")
test = [0,0,1,2,2,4,2,6,4,6,4]
loop_count = 0

def add_num(res,val):
    global filled
    global rate
    rate[res] = [val, val_buf[res], res/val]
    res_sorted = int("".join(sorted(str(val), reverse = True)))
    if set(list(str(res))) == set(list(str(val))):
        if sorted_vals[res_sorted] is None:
            sorted_vals[res_sorted] = [res]
        else:
            sorted_vals[res_sorted].append(res)
    filled += 1

def verify():
    ver_val = [0]*100001
    correct = 0
    wrong = 0
    with open("problem069.verify.txt","r") as fp:
        lines = fp.readlines()
        for line in lines:
            if line.strip() != '':
                x,rho = [int(i) for i in line.strip().split()]
                ver_val[x] = rho
    for i in range(1,min(len(ver_val), limit)):
        if ver_val[i] != rate[i][0]:
            wrong += 1
            print("ERROR for value", i, ": should have",ver_val[i],", have",rate[i][0])
        else:
            correct += 1
    print("Correct:", correct, "wrong:", wrong)
    if wrong > 0:
        quit()

stats = [0,0,0,0,0]

def prime_factors(x):
    global primes
    global prime_factor_cache
    global stats
    remainder = x
    prime_factors = set([])
    max_prime = 0
    stats[4] += 1

    for prime in primes:
        #print("--",prime)
        stats[0] += 1
        if remainder < prime:
            stats[1] += 1
            break
        if prime_factor_cache[remainder]:
            stats[2] += 1
#            print (x, prime, ": cache hit @", remainder," - ", prime_factor_cache[remainder], prime_factors)
            prime_factors.update(prime_factor_cache[remainder])
            break
        elif remainder % prime == 0:
            stats[3] += 1
#            print (x, prime, "modulo")
            prime_factors.add(prime)
            remainder //= prime
    prime_factor_cache[x] = prime_factors
    return prime_factors


print ("calculating prime factors")
for i in range(3,limit+1):
    phi = i
    for fact in prime_factors(i):
        phi *= val_buf[fact]
    add_num(i,int(phi))
print ("Runs: {}, loops/run: {:1f}, cache hits/run: {}, modulo/run {}".
        format(stats[4], stats[0]/stats[4], stats[2]/stats[4], stats[3]/stats[4]))

print ("verify result")
#verify()

print ("find min")
min_n_phi = 10
min_val = 0
for i in range(2,len(sorted_vals)):
    if sorted_vals[i] is not None:
#        print ("entry:", i, sorted_vals[i])
        for v in sorted_vals[i]:
#            print ("match", v, rate[v][2], "current min", min_n_phi)
            if rate[v][2] < min_n_phi:
                min_val = v
                min_n_phi = rate[v][2]

print (min_val, min_n_phi, rate[min_val])
