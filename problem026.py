# A unit fraction contains 1 in the numerator. The decimal representation of the unit
# fractions with denominators 2 to 10 are given:
#
# 1/2	= 	0.5
# 1/3	= 	0.(3)
# 1/4	= 	0.25
# 1/5	= 	0.2
# 1/6	= 	0.1(6)
# 1/7	= 	0.(142857)
# 1/8	= 	0.125
# 1/9	= 	0.(1)
# 1/10	= 	0.1
# Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen
# that 1/7 has a 6-digit recurring cycle.
#
# Find the value of d < 1000 for which 1/d contains the longest recurring cycle in
#its decimal fraction part.-

#see https://oeis.org/A051626

def A051626(n):
  lpow=1
  while True:
    for mpow in range(lpow-1, -1, -1):
      if (10**lpow-10**mpow) % n == 0:
        return lpow-mpow
    lpow += 1

longest_repeat=0
longest_num=1
for i in range (1, 1000):
  digits= A051626(i)
  print i, digits
  if longest_repeat < digits:
      longest_repeat = digits
      longest_num = i


print longest_num, longest_repeat
