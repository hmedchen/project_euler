#The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.
#We shall consider fractions like, 30/50 = 3/5, to be trivial examples.
#There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.
#If the product of these four fractions is given in its lowest common terms, find the value of the denominator.

def shorten(a):
  s=[a[0],a[1]]
  for i in range (2,9):
    if a[0]%i == 0 and a[1]%i == 0:
      s=[a[0]/i,a[1]/i]
  return (s)

matches=[]
for den in range (12,99):
  d=[float(den//10),float(den%10)]
  if d[1] == 0:             #exclude trivial cases
    continue
  for num in range (11,den):
    n=[float(num//10),float(num%10)]
    if n[0] == n[1] or n[1] == 0:  #exclude double digits and trivial cases
      continue
    if d[0] in n:
      # only cross numbers are possible.
      if n[1] == d[0] and n[0]/d[1] == num/den:
        matches.append([n[0],d[1]])
    if d[1] in n:
      if n[0] == d[1] and n[1]/d[0] == num/den:
        matches.append([n[1],d[0]])

m_num=1
m_den=1
for match in matches:
  m_num*=match[0]
  m_den*=match[1]

print (int(shorten([m_num, m_den])[1]))
