#and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
#
#Find the sum of the digits in the number 100!

target=100

x=1
for i in range (target,0,-1):
  x *= i
  print i, x

sum=0
for digit in str(x):
  sum += int(digit)

print sum
