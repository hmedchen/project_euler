# 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
#
# What is the sum of the digits of the number 2^1000?

b=2
exp=1000
x=b

for i in range (1,exp):
  x *= b
print "%d^%d=%d:" %(b,exp,x)

y=0
sx=str(x)
for pos in sx:
  y += int(pos)
print y
