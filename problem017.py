# If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
#
# If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
#
#
# NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.

target=1000
count=0

ones=[""]*20
tens=[""]*10

ones[0]=""
ones[1]="one"
ones[2]="two"
ones[3]="three"
ones[4]="four"
ones[5]="five"
ones[6]="six"
ones[7]="seven"
ones[8]="eight"
ones[9]="nine"
ones[10]="ten"
ones[11]="eleven"
ones[12]="twelve"
ones[13]="thirteen"
ones[14]="fourteen"
ones[15]="fifteen"
ones[16]="sixteen"
ones[17]="seventeen"
ones[18]="eighteen"
ones[19]="nineteen"

tens[0]=""
tens[1]=""
tens[2]="twenty"
tens[3]="thirty"
tens[4]="forty"
tens[5]="fifty"
tens[6]="sixty"
tens[7]="seventy"
tens[8]="eighty"
tens[9]="ninety"

def spell_num(num):
  len_words=0
  x=str(num)
  if len(x) > 0:
    if len(x) >= 2:
      if len(x) >= 3:
        if len(x) >= 4:
          if len(x) > 4:
            print ("numbers this large are not supported.")
            quit()
    # thousands
          len_words += len(ones[int(x[-4])])
          print ones[int(x[-4])],
          len_words += len("thousand")
          print "thousand",
    # hundrets
        if int(x[-3:]) > 0:
          len_words += len(ones[int(x[-3])])
          print ones[int(x[-3])],
          len_words += len("hundred")
          print "hundred",
          #if x[-2] != "0" or x[-1] != "0":
          if int(x[-2:]) > 0:
            len_words += len("and")
            print "and",
    # tens and ones
      if int(x[-2]) > 1:
        len_words += len(tens[int(x[-2])])
        print tens[int(x[-2])],
        if int(x[-2:]) % 10 != 0:
          print "-",
      else:
      # tens under 20
        len_words += len(ones[int(x[-2:])])
        print ones[int(x[-2:])]
    if int(x[-2:]) >= 20 or len(x) < 2:
      len_words += len(ones[int(x[-1])])
      print ones[int(x[-1])]

  return (len_words)

#  print len(x)
num_alpha = 0
for n in range (1, target+1):
  wordlen = spell_num(n)
  num_alpha += wordlen
print num_alpha
