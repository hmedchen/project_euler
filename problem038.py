# Take the number 192 and multiply it by each of 1, 2, and 3:
#
# 192 × 1 = 192
# 192 × 2 = 384
# 192 × 3 = 576
# By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)
#
# The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).
#
#What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?


start=2
end=9999   #(9999 concat 9999*2=19998 = 999919998, pretty high 9 digit number)

loop_end=10


def is_pandigital(arr,strict=False):
  num = int(''.join(arr))
  digit_avail=[True]*10
  digit_avail[0]=False

  strnum=str(num)
  for char in strnum:
    if digit_avail[int(char)] == False:
      return False
    digit_avail[int(char)]=False

  if strict == True:
    for d in digit_avail:
      if d == True:
        return False

  return True

best_candidate = 0
for i in range (start, end):
  i_arr=[]
  i_arr.append(str(i))
  multiplier=2
  pan_multis=True
  while True:
    i_arr.append(str(i*multiplier))
    if is_pandigital(i_arr) == False:
      pan_multis=False
      break
    if len(''.join(i_arr)) > 9:
      pan_multis=False
      break
    elif len(''.join(i_arr)) == 9:
      break
    multiplier += 1
  if pan_multis == True:
    print (i, i_arr, ''.join(i_arr))
    val=int(''.join(i_arr))
    if val > best_candidate:
      best_candidate = val
print (best_candidate)
