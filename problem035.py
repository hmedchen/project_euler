#The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
#
#There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
#
#How many circular primes are there below one million?

from common_funcs import *
import math

primes=find_primes_below (1000000)

def rotate_int (num):
  numlen=len(str(num))
  overflow=num%10
  rest=num//10
  return (overflow*(10**(numlen-1)) + rest )

def check_in_list (cand):
  global primes
  for prime in primes:
    if cand < prime:
      return False
    elif cand == prime:
      return True


sum_circ_primes=0
for prime in primes:
  candidate=prime
#  print (candidate)
  iscircular=True
  for i in range (1,len(str(candidate))):
    candidate=rotate_int(candidate)
    #if check_in_list(candidate) == False: # interestingly enough a proper prime check is quicker than looking up in the list
    if is_prime(candidate) == False:
        iscircular=False
        break
  if iscircular == True:
    sum_circ_primes += 1
    print ("yes:", prime, sum_circ_primes)

#  print (i)
