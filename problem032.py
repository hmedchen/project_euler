#We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once;
# for example, the 5-digit number, 15234, is 1 through 5 pandigital.
#
#The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier,
# and product is 1 through 9 pandigital.
#
#Find the sum of all products whose multiplicand/multiplier/product identity can be written
# as a 1 through 9 pandigital.
#
#HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum

# not quite right,
from common_funcs import *
import math

#digits=[0,1,2,3,4,5,6,7,8,9]  # remember 0 is not allowed

max=987654321
end = 9876
print(end)
#end2 = int(math.floor(987654321/3))
#end = 31426

products=[]
sum=0
for i in range (1,end):
    upper=int(end/i)
    for j in range (i+1,upper):
      m = int(i * j)
      combined=int(str(i)+str(j)+str(m))
      if len(str(combined)) == 9 and is_pandigital(combined):
        if not m in products:
          sum += m
          products.append(m)
        print (i,"*",j,"=",m,sum,combined)

print (products, sum)
