from common_funcs import *
import math

primepos=10001

maxprime = 2* primepos * math.log(primepos)

primes = find_primes_below(int(maxprime))
while len(primes) < primepos:
  maxprime *=2
  print maxprime
  primes = find_primes_below(int(maxprime))

print primes[10001]
