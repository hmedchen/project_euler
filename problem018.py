# By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
#
# (see .data1)
#
# That is, 3 + 7 + 4 + 9 = 23.
#
# Find the maximum total from top to bottom of the triangle below:
#
# (see .data2)
#
# NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)

# solution sketch:
# - bottom to top, record a link to the corresponding children
# - record the cost of the corresponding subtrees, and the most expensive of the two
# Example (+ represents more expensive, - less):
#
#          3
#        20+19-
#       7     4
#     10-13+13-15+
#    2     4     6
#  8+ 5- 5-  9+ 9+ 3-
# 8     5     9     3

class Tree:
  value=0
  subtree=[False,False]
  expensive_path=-1
  expensive_cost=0

  def __init__ (self, value, tree1=False, tree2=False):
    self.value=value
    if isinstance(tree1, Tree) and isinstance(tree2,Tree):
      self.subtree[0]=tree1
      self.subtree[1]=tree2
      if self.subtree[0].cost() > self.subtree[1].cost():
        self.expensive_path = 0
      else:
        self.expensive_path = 1
      self.expensive_cost = self.subtree[self.expensive_path].cost()

  def cost(self):
    if self.expensive_path != -1:
      return (self.expensive_cost + self.value)
    else:
      return (self.value)


def read_file(path):
  triangle=[]
  for line in reversed(list(open(path))):
    triangle.append(line.split())

  return triangle

def print_tree(tree):
  for line in tree:
    for col in line:
      print col.value, col.expensive_path, col.expensive_cost, col.cost(),"|",
    print


#---------- main ---------
#triangle = read_file("./problem018.data1")
triangle = read_file("./problem018.data2")
triangle = read_file("./problem067.data")
print triangle

tree=[]

for line in range (0,len(triangle)):
  treeline = []
  for col in range (0,len(triangle[line])):
    if line == 0:
      treeline.append(Tree(int(triangle[line][col])))
    else:
      treeline.append(Tree(int(triangle[line][col]),tree[line-1][col],tree[line-1][col+1]))
  tree.append(treeline)

print_tree(tree)
print "Highest Cost:" , tree[-1][-1].cost()
#for x in row:
#  for y in x:
#    print y.value, y.expensive_path, y.expensive_cost, y.cost(),"|",
#  print
