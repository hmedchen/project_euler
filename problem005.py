# 2520 is the smallest number that can be divided by each of the numbers
# from 1 to 10 without any remainder.
#
# What is the smallest positive number that is evenly divisible
# by all of the numbers from 1 to 20?

upper_limit=20

start = (upper_limit * (upper_limit-1))

cand = start

found = False
while found == False:
  for i in range (upper_limit, 11, -1):
    if cand % i != 0:
      break;
  else:
    print cand
    found = True

  #print cand
  cand += upper_limit
