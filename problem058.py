import common_funcs

maxnum=1
ratio = 1
prime_limit = 10000000

num_diag=1
num_p=0
pindex = 0
linelen = 1

while ratio > 0.1:
    linelen += 2

    b =  maxnum + (linelen-1)
    c =  maxnum + 2*(linelen-1)
    d =  maxnum + 3*(linelen-1)
    a =  maxnum + 4*(linelen-1)
    maxnum += 4*(linelen-1)
    num_diag += 4

    for corner in [a,b,c,d]:
        if common_funcs.is_prime(corner): num_p += 1

    ratio = num_p/num_diag
#    print ("max val: {} Side len: {} - ratio = {} / {} = {}".format(maxnum,linelen,num_p,num_diag,ratio))
print (linelen)
