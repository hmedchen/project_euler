# find the sum of all primes below two million
# sum of all primes below 10 is 2+3+5+7=17
import sys
import math
#limit=2000000
limit=2000000

def find_next_prime(x):
  limit=int(math.ceil(math.sqrt(x)))
  while True:
    if x >= 3:
      x +=2
    else:
      x +=1
    for div in range (3,limit+1,2):
      if x % div == 0:
        break
    else:
      return x


sum=2
prime=2
while True:
  prime=find_next_prime(prime)
  if prime <= limit:
    sum += prime
#    print prime,
  else:
    print "Sum: %d" % sum
    break
