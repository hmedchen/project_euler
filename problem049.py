# The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases
# by 3330, is unusual in two ways:
#     (i) each of the three terms are prime, and,
#     (ii) each of the 4-digit numbers are permutations of one another.
#
# There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes,
# exhibiting this property, but there is one other 4-digit increasing sequence.
# What 12-digit number do you form by concatenating the three terms in this
# sequence?

import common_funcs

def count_increases(key, list):
    for i in range(len(list)):
        for j in range(len(list)):
            if j != i:
                dist = abs(list[j] - list[i])
                for k in range(len(list)):
                    if list[k] - list[j] == dist and k != i:
                        print(str(list[i]) + str(list[j]) + str(list[k]), dist)


# create prime list
primes = common_funcs.find_primes_below(10000)
perm_primes = {}
for i in range(len(primes)-1):
    if primes[i] > 1000:
        primes = primes[i:]
        break

# group primes by permutation
for prime in primes:
    permuted = ''.join(sorted(str(prime)))
    if permuted in perm_primes.keys():
        perm_primes[permuted]['counter'] += 1
        perm_primes[permuted]['candidates'].append(prime)
    else:
        perm_primes[permuted] = {'counter': 1, 'candidates': [prime]}

# count increase for each candidate with >3 permutations
for key, pps in perm_primes.items():
    if pps['counter'] >= 3:
        count_increases(key, pps['candidates'])
