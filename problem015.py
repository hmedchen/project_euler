#Starting in the top left corner of a 2x2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
#How many such routes are there through a 20x20 grid?

grid_size = 20 + 1

g=[[1]*grid_size]

print g

for row in range (1,grid_size):
  g.append([1])
  for col in range (1,grid_size):
    g[row].append(g[row-1][col]+g[row][col-1])
  print row, g[row]

print g[-1][-1]
