# Prime digit replacements
# Problem 51
#
# By replacing the 1st digit of the 2-digit number *3, it turns out that six of
# the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.
#
# By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit
# number is the first example having seven primes among the ten generated numbers,
# yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993.
# Consequently 56003, being the first member of this family, is the smallest
# prime with this property.
#
# Find the smallest prime which, by replacing part of the number (not necessarily
# adjacent digits) with the same digit, is part of an eight prime value family.

# REMARK
# No idea how to do that yet - I am grouping the primes by length now,
# and try to calculate by how many digits they differ (just n->n+1 though,
# which is useleess)
# Maybe try with a trie instead?
import numpy
import common_funcs
NUM_MATCHES = 8

sieve = common_funcs.prime_sieve(1000000)
primes = list(numpy.flatnonzero(sieve))

cand={str(i):[] for i in range(10)}

def rep_nums(ref,a,b,to_rep):
    lref=list(ref)
    for n in range(to_rep):
        try:
            ind = lref.index(a)
        except:
            print(lref,a)
            raise
        lref[ind] = b
    return "".join(lref)

res=[]
for to_rep in range(1,4):
    for i in ("0123"):
        matches = {}
        for ref in primes[1200:]:
            sref = str(ref)
            if sref.count(i) >= to_rep:
                matches[ref]=[ref]
                for j in ("0123456789"):
                    if j != i:
                        totry = int(rep_nums(sref,i,j,to_rep))
                        if sieve[int(totry)] and len(str(totry)) == len(sref) and totry>ref:
                            matches[ref].append(totry)
                if len(matches[ref]) >= NUM_MATCHES:
                    print(ref, to_rep, len(matches[ref]),matches[ref])
                    res.append(ref)
                    continue

print(sorted(res))
