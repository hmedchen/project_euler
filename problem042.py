#The nth term of the sequence of triangle numbers is given by,
# tn = ½n(n+1); so the first ten triangle numbers are:
#
#1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
#
#By converting each letter in a word to a number corresponding
# to its alphabetical position and adding these values we form a word value. For example,
# the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.
#
#Using words.txt (right click and 'Save Link/Target As...'), # a 16K text file containing
# nearly two-thousand common English words, how many are triangle words?

import json

# get words
fp = open("problem042.words","r")
content="["+fp.read()+"]"
words=json.loads(content)

# calc first couple of triangles
trilvl=20
tri=[0]*trilvl
for i in range (2,trilvl+2):
  tri[i-2]=int(0.5*i*(i-1)) #print (words)

# calc offset to letters for easy conversion
offset=ord('A')-1

wc=0
for word in words:
  value=0
  for letter in word:
    value += ord(letter)-offset
  if value in tri:
    wc +=1
print (wc)
