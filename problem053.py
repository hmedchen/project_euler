#There are exactly ten ways of selecting three from five, 12345:
#
#123, 124, 125, 134, 135, 145, 234, 235, 245, and 345
#
#In combinatorics, we use the notation,
#.
##
#In general,
#
#, where , , and .
#
#It is not until , that a value exceeds one-million:
#.
#
#How many, not necessarily distinct, values of
# for , are greater than one-million?


# Pascal's triangle

max=101
THRESHOLD = 1000000

pasc=[[1]*max for _ in range(max)]
for row in range(1,max):
    for col in range(1,max):
        pasc[row][col]=pasc[row-1][col]+pasc[row][col-1]

def get_pasc(upper, lower):
    return(pasc[lower][upper-lower])

#print(get_pasc(23,10))
num_matches=0

for upper in range(1,max):
    for lower in range(1,upper):
        p = get_pasc(upper,lower)
        if p > THRESHOLD:
#            print(upper, lower, p)
            num_matches+=1
print(num_matches)
