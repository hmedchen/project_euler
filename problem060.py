# The primes 3, 7, 109, and 673, are quite remarkable. By taking any two primes
# and concatenating them in any order the result will always be prime.
# For example, taking 7 and 109, both 7109 and 1097 are prime. The sum of these
# four primes, 792, represents the lowest sum for a set of four primes with this
# property.

# Find the lowest sum for a set of five primes for which any two primes
# concatenate to produce another prime.
import common_funcs

upper_bound = 10000
max_poss_prime = 100000000

print ("Allocating", max_poss_prime/1024/1024, "MB!")
primes_sieve = common_funcs.prime_sieve(max_poss_prime)
print("Primes calculated.")

paircache={}

def check_concat(p):
    new = p[-1]
    if p[-2] in paircache and paircache[p[-2]][new-p[-2]]:
        return False
    for old in p[:-1]:
        cands = [new*10**len(str(old))+old, old*10**len(str(new))+new]
        for cand in cands:
            if cand <= max_poss_prime:
                if not primes_sieve[cand]:
                    if not old in paircache.keys():
                        paircache[old]=[False]*(upper_bound-old)
                    paircache[old][new-old] = True
                    return False
            else:
                print ("OVERFLOW!")
                quit()
    return True

def check_next_level(cand):
    newlist = []
    for next in range(cand[-1]+1,upper_bound):
        if primes_sieve[next] and check_concat(cand+[next]):
            newlist += [cand+[next]]
    return newlist

def find_chains(upper_bound):
    global primes_sieve
    lastprime=0
    a = 0
    while lastprime < upper_bound:
        # first prime
        a = primes_sieve.index(True,lastprime+1)
        for b in range(a+1,upper_bound):
            # second prime
            if primes_sieve[b]:
                if check_concat([a,b]):
                    # third prime
                    cands = check_next_level([a,b])
                    # forth prime
                    for cand in cands:
                        cands3 = check_next_level(cand)
                        # fifth prime
                        for cand3 in cands3:
                            print(cand3)
                            cands4 = check_next_level(cand3)
                            if cands4 != []:
                                return(cands4)
        lastprime = a
    return []

newcands = find_chains(upper_bound)

if len(newcands) > 0:
    print ("Sum:",sum(newcands[0]))
else:
    print ("No solution")
print("Upper bound:", upper_bound)
print(len(paircache))
