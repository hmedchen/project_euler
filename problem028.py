# Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:
#
# 21 22 23 24 25
# 20  7  8  9 10
# 19  6  1  2 11
# 18  5  4  3 12
# 17 16 15 14 13
#
# It can be verified that the sum of the numbers on the diagonals is 101.
#
# What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?

#1  3 5 7 9    13 17 21 25   31 37 43 49
#1   diff 2      diff 4         diff 6

# num groups = width-1 /2 +1

#target=5
target=1001
width = int((target-1)/2)

print ("Width: ", width)

x=1
diff=2
sum=1

for circle in range (0,width):
  for i in range (0,4):
    x += diff
    sum += x
#    print (x, sum)
  diff +=2

print (sum)
