#If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.
#
#{20,48,52}, {24,45,51}, {30,40,50}
#
#For which value of p ≤ 1000, is the number of solutions maximised?
import math

def find_matching_triangles(perimeter):
 # (s-a)*(s-b)=s(s-c)
 # a*a+b*b = c*c
 # a<=b<c
  s=math.floor(perimeter/2)
  num_solutions=0
  # the end range could probably be further improved
  for a in range (1,s):
    aa=a*a
#    print (a,end=" ")
    per_a=perimeter-a
    for b in range (a,s):
      bb=b*b
      c=per_a-b
      cc=c*c
      aabb=aa+bb # cutting additions in half
      if aabb < cc:
        continue
      elif aabb > cc:
        break
      else:
#        print ("--", a,b,c)
        num_solutions += 1

  return num_solutions


max=0
for i in range (1,1000):
  num = find_matching_triangles(i)
  if num > max:
    max = num
    print (i, max)

print (max)
