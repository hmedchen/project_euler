limit=1000000

cache=[False]*(limit+1) #fixed memory limit,
max_num=0
max_len=0

for i in range (2, limit):
  len=1
  n=i
  while n > 1:
    # read from cache if it's int he cache size and not empty
    if n <= limit and cache[n] != False:
      len += cache[n] -1
      break
    # else, calculate
    if n % 2 == 0:
      n /= 2
    else:
      n= 3*n + 1
    len += 1
  cache[i] = len    # write result of new item into the cache
  if len > max_len:
    max_num = i
    max_len = len

#print cache [0:limit]
print "Biggest sequence starts with %d - length: %d" %(max_num, max_len)
