#The prime factors of 13195 are 5, 7, 13 and 29.
#
#What is the largest prime factor of the number 600851475143 ?

target=600851475143
#target=13195

# requested num is too big for range()
p=2
while p < target:
    prime = True
    if target % p == 0:
      cand = target / p
      print ("candidate: %d" % (cand))
      i=2
      while i <  cand:
        if cand % i == 0:
          prime = False
          break
        i += 1

      if prime == True:
        print (" biggest prime factor: %d" %(cand))
        break
    p += 1

else:
    print "target is prime"
