import math
import numpy

def get_prime_sieve_below(limit):
    crosslimit=int(math.floor(math.sqrt(limit))) #limit for prime factorization
    sieve = [True] * limit #binary array which marks eliminated candidates
    sieve[0] = sieve [1] = False
    for n in range (4, limit,2):
        sieve[n]=False     #eliminate all even numbers >2
    for n in range (3, crosslimit, 2):
        if sieve[n]:
            for m in range (n*n, limit, 2*n): # (could it be 2n instead of n^2?)
                sieve[m] = False   # eliminate multiples of x
    return sieve

# using Numpy. 10 times as fast as the above!!
def prime_sieve(n):
    flags = numpy.ones(n, dtype=bool)
    flags[0] = flags[1] = False
    flags[4::2]=False     #eliminate all even numbers >2
    for i in range(3, int(n**0.5)+1, 2):
        # We could use a lower upper bound for this loop, but I don't want to bother with
        # getting the rounding right on the sqrt handling.
        if flags[i]:
            flags[i*i::i] = False
    return list(flags)
#    return numpy.flatnonzero(flags)

def find_primes_below(limit):
  primes=[]
  if limit < 2: return []
  sieve = prime_sieve(limit)
  return list(numpy.flatnonzero(sieve))

def is_prime (x):
    if type(x) == str and not x.is_integer():
        return(False)
    if x < 2:
      return (False)
    if x <= 3:
      return (True)
    if x % 2 == 0:
      return (False)
    for i in range (3, math.ceil(math.sqrt(x))+1, 2):
      if x % i == 0: return (False)
    return (True)

def get_divisors(x):
# timing for x=200: 2m20s
  divisors=[1]
  end=int(math.sqrt(x))
  num_div = 1
  for i in range (2,end+1):
    if x % i == 0:
      divisors.append(i)
      if not int(x/i) in (divisors):
        divisors.append(int(x/i))
      #num_div += 2 # in case of x/i=y I would cound i and y
  divisors.append(x)
  return (sorted(divisors))

def get_num_divisors(x):
  count = 1
#  if x <=2: return x
  primes=find_primes_below(int(x//2))
 # primes=find_primes_below(int(math.sqrt(x)))
  for prime in primes:
    if prime*prime > x: # halt for primes > sqrt(triangle_num)
      count *= 2
      break

    exponent = 1
    while x % prime == 0:   # check highest exponent
      exponent +=1
      x /= prime
    if exponent > 1:
      count *= exponent
    if x == 1:
      break
  return (count)

def get_digit(n,pos):
  return n // 10 ** pos % 10

def is_palindromic (cand):
  i=str(cand)
  for x in range (0,int(len(i)/2)):
    if i[x] != i[-1-x]:
      return False
  return True

def is_pandigital(num,strict=False):
  digit_avail=[True]*10
  digit_avail[0]=False

  strnum=str(num)
  for char in strnum:
    if digit_avail[int(char)] == False:
      return False
    digit_avail[int(char)]=False
  if strict == True:
    for d in digit_avail:
      if d == True:
        return False
  # check 1-x
  for i in range (1,10):
    if digit_avail[i] == True:
      break
  if i < len(str(num)):
    return False
  return True
