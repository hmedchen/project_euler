#The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.
#Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
#
#NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.

# -- there must be a catch to that... size of primes?
#try brute force aproach.
limit=1000000  # good guess, exact fit :)

from common_funcs import find_primes_below, is_prime

primes=find_primes_below(limit)

#print (is_prime(9))
#quit()

sum_primes = 0
while primes[0] < 10:
  primes.pop(0)

for prime in primes:
  cand=prime
  is_trunc_prime=True

  # first round, trunc right
  cand = cand // 10
  while cand > 0:
    if not is_prime(cand):
      is_trunc_prime=False
      break
    cand = cand // 10
  if is_trunc_prime == False:
    continue
  cand=prime
  # 2nd round, trunc left
  cand = cand % 10**(len(str(cand))-1)
  while cand > 0:
    prime_len = len(str(cand))
    if not is_prime(cand):
      is_trunc_prime=False
      break
    cand = cand % 10**(prime_len-1)
  if is_trunc_prime == True:
    sum_primes += prime
    print (prime, "YES")

print ("sum:", sum_primes)
