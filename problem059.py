#Each character on a computer is assigned a unique code and the preferred standard
#is ASCII (American Standard Code for Information Interchange).
#For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.
#
#A modern encryption method is to take a text file, convert the bytes to ASCII,
#then XOR each byte with a given value, taken from a secret key. The advantage
#with the XOR function is that using the same encryption key on the cipher text,
# restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.
#
#For unbreakable encryption, the key is the same length as the plain text message,
#and the key is made up of random bytes. The user would keep the encrypted message
# and the encryption key in different locations, and without both "halves", it
# is impossible to decrypt the message.
#
#Unfortunately, this method is impractical for most users, so the modified method
# is to use a password as a key. If the password is shorter than the message,
# which is likely, the key is repeated cyclically throughout the message. The
# balance for this method is using a sufficiently long password key for security,
# but short enough to be memorable.
#
#Your task has been made easy, as the encryption key consists of three lower case
# characters. Using p059_cipher.txt, a file containing the encrypted ASCII codes,
# and the knowledge that the plain text must contain common English words, decrypt
# the message and find the sum of the ASCII values in the original text.
import itertools

cypher=[]
pass_len = 3
pass_alpha = "abcdefghijklmnopqrstuvwxyz"

ld = {c:0 for c in pass_alpha}

valid_texts = []
num_alphanums = []
valid_text_dicts = []

def xor(a,b):
    ''' simple xor func '''
    plain=''
    for x in range(3):
        xored = [a[0] ^ b[0], a[1] ^ b[1], a[2] ^ b[2]]
    return (xored)

# get cyphered text
with open("problem059.cypher.txt") as fp:
    line=fp.readline().strip()
    cypher=[int(i) for i in line.split(",")]
print("Cypher length:", len(cypher), "%3:",len(cypher)%3)

# some counters
valids = 0
num_tries = 0

# list of banned characters, to limit the passwords to try
banned = [[''],[''],['']]
# generate full list of permutations, will sieve out in main loop
password_list = ["".join(i) for i in list(itertools.permutations(pass_alpha, pass_len))]

for pwd in password_list:
    # Break if character is for sure not valid
    if pwd[0] in banned[0] or pwd[1] in banned[1] or pwd[2] in banned[2]:
        continue
    else:
        num_tries += 1

    letter_dist = ld.copy()
    plain=""
    valid = True

    # first checkpoint to sieve out
    asc_pwd = [ord(p) for p in pwd]
    xored = xor(cypher[0:3], asc_pwd)

    # ASSUMPTIONS:
    # - first character is upper
    # - second character is lower
    # - third character is lower or interpunctation or space
    a=chr(xored[0])
    if not a.isupper():
        if a not in banned[0]:
            banned[0].append(pwd[0])
        continue
    a=chr(xored[1])
    if not a.islower():
        if a not in banned[1]:
            banned[1].append(pwd[1])
        continue
    a=chr(xored[2])
    if not (a.islower() or a in " ,;.!?"):
        if a not in banned[2]:
            banned[2].append(pwd[2])
        continue

    for n in range(0,len(cypher),3):
        xored = "".join([chr(p) for p in xor(cypher[n:n+3], asc_pwd)])
        if xored.isprintable():
            for x in [i for i in xored if i.isalpha()]:
                letter_dist[x.lower()] += 1

            plain += xored
        else:
            for i in range(3):
                if not xored[i].isprintable():
                    banned[i].append(pwd[i])
            valid = False
            break

    if valid:
        valids += 1
#        print(letter_dist)
        valid_texts.append(plain)
        valid_text_dicts.append(letter_dist)
        num_alphanums.append(sum([int(i.isalpha()) for i in plain]))

print("Possible passwords:", len(password_list), "Tried:", num_tries )
print("Valid texts:", valids)
print("Number of Letters in valid texts", sorted(num_alphanums,reverse=True))
bestmatch = num_alphanums.index(max(num_alphanums))
print("Best Match (most letters):")
print(valid_texts[bestmatch])
print("Letter histogram:", dict(sorted(valid_text_dicts[bestmatch].items(), key=lambda item: item[1], reverse=True)))
print("ASCII sum:", sum([ord(c) for c in valid_texts[bestmatch]]))
