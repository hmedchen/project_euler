#145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
#Find the sum of all numbers which are equal to the sum of the factorial of their digits.
#Note: as 1! = 1 and 2! = 2 are not sums they are not included.

import math

# As we will only need 9 different factorials, calc them up front
fact=[math.factorial(n) for n in range (0,10)]

print (fact)

max=0
for i in fact:
  max +=i

print ("max value:",max)
result=0
for candidate in range (3,max):
  sum=0
  toobig = False
  ca=[int(x) for x in str(candidate)]
  for i in range (0,len(ca)):
    sum += fact[ca[i]]
    if sum > candidate:
      toobig=True
      break
  if toobig == True:
    continue
  if sum == candidate:
    result += sum
    print (sum)

print ("result:", result)
