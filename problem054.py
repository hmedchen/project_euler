# https://projecteuler.net/problem=54
# Poker hands
# ---- range---
# High Card: Highest value card.
# One Pair: Two cards of the same value.
# Two Pairs: Two different pairs.
# Three of a Kind: Three cards of the same value.
# Straight: All cards are consecutive values.
# Flush: All cards of the same suit.
# Full House: Three of a kind and a pair.
# Four of a Kind: Four cards of the same value.
# Straight Flush: All cards are consecutive values of same suit.
# Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
CARD_ORDER="23456789TJQKA"
CARD_SUITS="HSDC"

# KONSTANTEN
HIGH=0
PAIR=1
TWOPAIR=2
TOAK=3
STRAIGHT=4
FLUSH=5
FH=6
FOAK=7
SFLUSH=8
RFLUSH=9

class Hand():
    def __init__(self,hand):
        self.hand = hand
        self.vals=[0]*13
        self.suits=[0]*4
        self.flush=False
        self.straight=False
        self.dupes=[[],[],[],[],[]]
        # Category = 0:high, 1:pair, 2:2pair, 3:trip, 4:straight
        #            5:flush, 6:fh, 7:4oak, 8:sflush, 9:rflush
        self.category=0
        self.handval=[0,0,0]
        self.get_stats()

    def __repr__(self):
        hand = [["High Card",[1]],
                ["Pair",[2]],
                ["Two Pairs",[2,2]],
                ["Three of a Kind",[3]],
                ["Straight",[1]],
                ["Flush",[1]],
                ["Full House",[3,2]],
                ["Four of a Kind",[4]],
                ["Straight Flush",[1]],
                ["Royal Flush",[1]]]
        handdisp = sorted(self.hand,key=lambda x:CARD_ORDER.index(x[0]))
        return("{}: {} ({}/{}) ".format(" ".join(handdisp),
                    hand[self.handval[0]][0].ljust(18),
                    CARD_ORDER[self.handval[1]],
                    CARD_ORDER[self.handval[2]] if self.handval[2]>=0 else "-"))

    def get_stats(self):
        # record values
        for card in self.hand:
            self.vals[CARD_ORDER.find(card[0])]+=1
            self.suits[CARD_SUITS.find(card[1])]+=1
        # duplicate stats
        for ci in range(len(self.vals)):
            self.dupes[self.vals[ci]].append(ci)
        # flush
        if max(self.suits) == 5: self.flush=True
        # straight
        if len(self.dupes[1]) == 5:
            x=[]
            self.dupes[1] = sorted(self.dupes[1], reverse=True)
            for c in range(5):
                x.append(self.dupes[1][c]+c)
            if min(x) == max(x):
                self.straight = True
        self.evaluate()

    def evaluate(self):
        if self.flush:
            if self.straight:
                if max(self.dupes[1]) == 12:
                    self.category = RFLUSH
                else:
                    self.category = SFLUSH
            else:
                self.category = FLUSH
        elif self.straight:
            self.category = STRAIGHT
        elif len(self.dupes[4]) > 0:
            self.category = FOAK
        elif len(self.dupes[3]) > 0:
            if len(self.dupes[2]) > 0:
                self.category = FH
            else:
                self.category = TOAK
        elif len(self.dupes[2]) == 2:
            self.category = TWOPAIR
        elif len(self.dupes[2]) == 1:
            self.category = PAIR
        else:
            self.category = HIGH

        self.handval[0] = self.category
        hand = [["High Card",[1,1]],
                ["Pair",[2,1]],
                ["Two Pairs",[2,2]],
                ["Three of a Kind",[3,1]],
                ["Straight",[1]],
                ["Flush",[1,1]],
                ["Full House",[3,2]],
                ["Four of a Kind",[4,1]],
                ["Straight Flush",[1]],
                ["Royal Flush",[1]]]
        h = hand[self.category]
        self.handval[1] = max(self.dupes[h[1][0]])
        n = -1
        if len(h[1]) == 2:
            if h[1][0] == h[1][1]:
                n = sorted(self.dupes[h[1][0]])[-2]
            else:
                n = max(self.dupes[h[1][1]])
        self.handval[2] = n

with open("problem054.data") as fp:
    win=[0,0]
    for play in fp:
        p1 = Hand(play.split()[:5])
        p2 = Hand(play.split()[5:])
        winner = 0
        for i in range(3):
            if p1.handval[i] > p2.handval[i]:
                winner = 1
                win[0]+=1
                break
            elif p1.handval[i] < p2.handval[i]:
                winner = 2
                win[1]+=1
                break
        print("{} {} {}, Player {} wins!".format(p1, "<- " if winner == 1 else " ->", p2, winner))
    print(win)
