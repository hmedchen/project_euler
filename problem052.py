# It can be seen that the number, 125874, and its double, 251748, contain
# exactly the same digits, but in a different order.
# Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x,
# contain the same digits.

import common_funcs

for r in range(2,7):
    maxval=int("9"*r)
    minval=int("1"+"0"*(r-1))
    print (maxval, maxval//6, minval)

    for cand in range(minval, maxval+1):
        allvals=set([i for i in str(cand)])
        match=True
        for mult in range(2,7):
            if set([i for i in str(cand*mult)]) != allvals:
                match=False
                break
        if match==True:
            print (cand)
            print (*[cand*i for i in range(7)])
