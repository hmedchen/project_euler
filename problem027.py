# Considering quadratics of the form:
#
# n^2+an+b, where |a|<1000|a|<1000 and |b|≤1000|b|≤1000
#
# where |n||n| is the modulus/absolute value of nn
# e.g. |11|=11|11|=11 and |−4|=4|−4|=4
# Find the product of the coefficients, aa and bb, for the quadratic
#expression that produces the maximum number of primes for consecutive values of nn, starting with n=0n=0.

# either both positive
# or one of them negative
#   b< an
from common_funcs import *

lim=999

max_a=0
max_b=0
run=0
for b in range((-1 * lim), lim+1):
  print (b)
  if b < 0:
    start_a = 0
  else:
    start_a = (-1 * lim)
  for a in range(start_a, lim+1, 2):
#    print (a,b, end=" | ")
    n=0
    num_primes=0
    while True:
      if is_prime (n*n + a*n + b) == True:
        num_primes +=1;
      else:
        if num_primes > run:
          max_a=a
          max_b=b
          run=num_primes
#        print (cand, run)
        break
      n +=1

print (max_a, max_b, max_a*max_b, run)
