# The first two consecutive numbers to have two distinct prime factors are:
#
# 14 = 2 × 7
# 15 = 3 × 5
#
# The first three consecutive numbers to have three distinct prime factors are:
#
# 644 = 2² × 7 × 23
# 645 = 3 × 5 × 43
# 646 = 2 × 17 × 19.
#
# Find the first four consecutive integers to have four distinct prime factors
#  each. What is the first of these numbers?

import common_funcs
import math

consec = 4
factors = 4
primes = common_funcs.find_primes_below(10000)


def check_prime_factors(n, num, prev=0):
    global primes
    for prime in primes:
        if prime > math.sqrt(n):
            return False
        remainder = n/prime
        if remainder.is_integer():
        #    print(prime, remainder)
            if prev == prime:
                newnum = num
            else:
                newnum = num - 1
            if newnum <= 1:
                if common_funcs.is_prime(remainder):
                    if int(remainder) != prime:
                        return [prime, int(remainder)]
            other_prime_facts = check_prime_factors(int(remainder), newnum, prime)
            if other_prime_facts is not False:
                return [prime] + other_prime_facts
            else:
                return False


series = 0

i = 3
while True:
    facts = check_prime_factors(i, factors)
    if facts is not False:
        series += 1
#        print(i, facts)
    else:
        series = 0
#    print(series)
    if series == consec:
        print (i - consec + 1)
        quit()
    i += 1
