# Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:
#
# 1634 = 1**4 + 6**4 + 3**4 + 4**4
# 8208 = 8**4 + 2**4 + 0**4 + 8**4
# 9474 = 9**4 + 4**4 + 7**4 + 4**4
# As 1 = 1**4 is not a sum it is not included.
#
# The sum of these numbers is 1634 + 8208 + 9474 = 19316.
#
# Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.

# As we will only need 9 different squares, calc them up front
exp = 5

pow=[n**exp for n in range (0,10)]

max=10**(exp+1) - 1 # seems to always fit - saves 200ms

# get nth digit from a number. nor sure if there is a better way.
# the calculation seems to be ~30% more effective over the string manipulation
def get_digit(n,pos):
#  return int(str(n)[pos])
  return n // 10 ** pos % 10

result=0
for candidate in range (pow[2],max):
#  print (candidate,end=": ")
  sum=0
  for i in range (0,len(str(candidate))):
    digit=get_digit(candidate,i)
#    print (pow[digit],end=" + ")
    sum += pow[digit]
#  print ("=",sum)
  if sum == candidate:
    result += sum
    print (sum)

print ("result:", result)
