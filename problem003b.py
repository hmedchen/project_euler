from common_funcs import *
import math

n=600851475143

primes = find_primes_below(int(math.sqrt(n)*2))
factors=[]

for p in primes:
  if n % p == 0:
    factors.append(p)
    n /= p;

print factors
