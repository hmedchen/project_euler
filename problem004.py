# A palindromic number reads the same both ways.
#The largest palindrome made from the product of
# two 2-digit numbers is 9009 = 91 * 99
#
# Find the largest palindrome made from the product of two 3-digit numbers.

from common_funcs import *
max_product=1000000
#max_product=10000
max_factor=999

def factorize (i, max):
  for f1 in range (max, 1, -1):
    for f2 in range (max, 1, -1):
      if i / f1 == f2 and i % f1 == 0:
        return (f1, f2)
  return (False)

for prod in range (max_product-2, 1, -1):
  if is_palindromic (int(prod)):
    fact=factorize (prod,max_factor)
    if fact != False:
      print (prod)
      print (fact)
      break
