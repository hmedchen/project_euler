pow = {i:i**3 for i in range(1,20000)}
base = {}

for p in pow:
    b = "".join(sorted(str(pow[p])))
    if b[0]!="-":
        if b in base:
            base[b].append(p)
        else:
            base[b]=[p]

smallest = 99999999999999
for p in base:
    if len(base[p]) >= 5:
        smallest = min(min([pow[n] for n in base[p]]), smallest)
        print(p,base[p],[pow[n] for n in base[p]])

print("Smallest entry:", smallest)
