
# PROBLEM:
# The candidates do not have to be in sequence!!!
# could be P3->P5->P4, just as in the example
# :-(


p={}
p[3]=[str(int(n*(n+1)/2)) for n in range(0,200)]
p[4]=[str(int(n**2)) for n in range(0,200)]
p[5]=[str(int(n*(3*n-1)/2)) for n in range(0,100)]
p[6]=[str(int(n*(2*n-1))) for n in range(0,100)]
p[7]=[str(int(n*(5*n-3)/2)) for n in range(0,100)]
p[8]=[str(int(n*(3*n-2))) for n in range(0,100)]
# some self tests
for k in p:
    print(k,":", *p[k][:50])
    print(k, p[k][-1])

print(p[3][127],"= 8128",p[4][91], "= 8281", p[5][44],"= 2882")
print(p[3].index("8128")," = 127", p[4].index("8281"), "= 91", p[5].index("2882"),"= 44")
print("")

def find_cyclic(a='',m=[3,4,5,6,7,8]):
    global p
    if len(m) == 6:
        n=3
        for p3 in p[3]:
            if len(p3) == 4  and p3[-2] != '0':
                x=m.copy()
                x.remove(3)
#                print(" "*(n-1)+str(n)+" "*(8-n), *a, p3)
                result = find_cyclic([p3],x)
                if len(result) > 0:
                    print("RESULT",result)
                    return result
        print("No Results.")
        return []
    elif len(m)<=1:
        cand = a[-1][2:]+a[0][:2]
        for n in m:
            if cand in p[n]:
                a.append(cand)
                print ("Complete Match: ", *a)
                return a
            return []
    else:
        for n in m:
            for pn in p[n]:
                if len(pn) == 4 and a[-1][2:] == pn[:2] and pn[-2]!='0' and pn not in a:
                    x=m.copy()
                    x.remove(n)
                    if len(a) > 3:
                        print(" "*(n-1)+str(n)+" "*(8-n), *a, pn)
                    retval =  find_cyclic(a+[pn],x)
                    if retval != []:
                        return retval
        return []
# recursive function from 3...9 returns True if value ABxx exitst for xxAB

out = find_cyclic()
print(out, "sum:", sum([int(i) for i in out]))
#for o in range(len(out)):
#    print("p{} index: {} ({})".format(o+3,p[o+3].index(out[o]),out[o]))
