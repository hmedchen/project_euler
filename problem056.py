#A googol (10100) is a massive number: one followed by one-hundred zeros;
# 100100 is almost unimaginably large: one followed by two-hundred zeros.
# Despite their size, the sum of the digits in each number is only 1.
#
#Considering natural numbers of the form, ab, where a, b < 100, what is the
# maximum digital sum?

#print(199*9)

def digisum(x):
    return sum([int(i) for i in str(x)])

maxsum = 0
for e in range(99,0,-1):
    for n in range(99,0,-1):
        cand = n ** e
        dsum = digisum(cand)
        if dsum > maxsum:
            maxsum = dsum
            print ("{}**{} = {}, len: {}, digital sum: {}".format(
                    str(n).rjust(3),str(e).rjust(3),cand,len(str(cand)), dsum))
