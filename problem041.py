#We shall say that an n-digit number is pandigital
#if it makes use of all the digits 1 to n exactly once.
#For example, 2143 is a 4-digit pandigital and is also prime.
#
#What is the largest n-digit pandigital prime that exists?

#tried to get all primes below 1000000000, but got a mem error (understandibly).
#trying to find pandigital now and then test for prime

# 9-digit pandigitals are not prime, as 1+2+3+4+5+6+7+8+9 = 45 (dividible by 3)
# same for 8 digits
from common_funcs import *

# brute force for 7 digits top down seems to be quickest
# let's use the highest odd, 7-digit pandigital number as start
for i in range (7654321,1234567,-2):
  if is_pandigital(i) == True:
    if is_prime(i) == True:
      print (i)
      quit()
