#find the 10.001st prime number

primes=[2]
primepos=10001

def find_next_prime(x):
  while True:
    x +=1
    for div in range (2,x):
      if x % div == 0:
       break
    else:
      return x

for z in range (1,primepos):
  primes.append(find_next_prime(primes[-1]))

print primes[-1]
print primes
