# In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:
#
# 1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
# It is possible to make £2 in the following way:
#
# 1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
# How many different ways can £2 be made using any number of coins?

target=200
coins=[200,100,50,20,10,5,2,1]

def howmany (target, coins, l=''):
  subpos = 0
  possibilities = 0
  remain=target
  max=int(target / coins[0])

  for thiscoin in range (0,max+1):
    remain=target - thiscoin * coins[0]
    if remain < 0:
      break
    elif remain == 0:
#      print (l, "all", target,"p can be covered with", thiscoin,coins[0],"p coins.")
      possibilities += 1
      break
    elif len(coins) > 1 and remain > 0:
      subpos = howmany(remain, coins[1:],l+" ") # 0 if it doesn't fit
      if subpos > 0: # there is a possibility
        possibilities += subpos
#        print (l, target,"p can be covered with", thiscoin,coins[0],"p coins. and", subpos," other possibilities")
  return (possibilities)

print ()
print ("Possibilities:",howmany (target, coins))
