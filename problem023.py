#A perfect number is a number for which the sum of its proper divisors is exactly equal to the number.
# For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means
# that 28 is a perfect number.
#A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant
# if this sum exceeds n.
#As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as
# the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater
#than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced
#any further by analysis even though it is known that the greatest number that cannot be expressed as the
# sum of two abundant numbers is less than this limit.
#Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
upper_limit=28123
an_sum=0

from common_funcs import *

def find_abundant_nums_below(x):
  nums=[]
  for i in range (12,x+1):
    sum_div=sum(get_divisors(i)[0:-1])
    if sum_div > i:
      nums.append(i)
  return (nums)

an= find_abundant_nums_below(upper_limit)
#print (an, len(an))

# New angle - sum up all the candidates into array and print everything that's not in the array
startidx = 0
sieve=[False]*(upper_limit+1)
for i1 in range (startidx,len(an)):
  for i2 in range (startidx,len(an)):
    result = an[i1] + an[i2]
    if result <= upper_limit:
      sieve[result] = True
    else:
      break
  startidx += 1

#add up the numbers that don't require a test
for n in range (1,upper_limit+1):
  if sieve[n] == False:
    an_sum += n
#    print ("no.", n, an_sum)

print (an_sum)
