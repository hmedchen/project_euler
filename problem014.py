# which collaz sequence starting number under 1.000.000  has the longest sequence

def collaz(n):
  len=1
  while n > 1:
    if n % 2 == 0:
      n /= 2
    else:
      n= 3*n + 1
    len += 1
  return (len)

len = 0
start = 0
for i in range (1,1000000):
  clen=collaz(i)
  if clen > len:
    len = clen
    start = i
  if i % 10000 == 0:
    print i

print start, len
