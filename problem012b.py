# based on projecteuler.net
# find the first triangle number with at least 500 divisors
# please read https://projecteuler.net/overview=012

from common_funcs import *

limit = 500
primes = find_primes_below (65500)

tri_num=1
increment=1
count = 0
#for count in range (0, limit):
while count <= limit:
  increment += 1
  tri_num += increment
  tt=tri_num
  count = 1
  for prime in primes:
    if prime*prime > tt: # halt for primes > sqrt(triangle_num)
      count *= 2
      break

    exponent = 1
    while tt % prime == 0:   # check highest exponent
      exponent +=1
      tt /= prime
    if exponent > 1:
      count *= exponent
    if tt == 1:
      break
  print tri_num, count

print tri_num
