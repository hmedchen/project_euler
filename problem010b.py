import math

limit=2000000

crosslimit=int(math.floor(math.sqrt(limit))) #limit for prime factorization
sieve=[False]*(limit+1) #binary array which marks eliminated candidates
for n in range (4,limit+1,2):
  sieve[n]=True     #eliminate all even numbers >2
for n in range (3, crosslimit, 2):
  if sieve[n] == False:
    for m in range (n*n, limit+1, 2*n): # (could it be 2n instead of n^2?)
      sieve[m] =True   # eliminate multiples of x

sum=0
for n in range (2,limit+1):
  if sieve[n] == False:
    sum += n

print sum
