import common_funcs
import math

i = 9
limit=10000

primes = common_funcs.find_primes_below(limit)
squares = [i**2 for i in range(1,limit)]
print(squares[0:20])

while True:
    if not common_funcs.is_prime(i):
    #if i not in primes:
        found = False
        for p in primes:
            if p > i-1:
                break
            halfrest = (i-p)  // 2
            sqbase = (math.sqrt(halfrest))
            if sqbase.is_integer():
                sqbase = int(sqbase)
                #print (i, "?=", p, " + 2* " ,sqbase, "^2")
                if halfrest in squares:
                    found = True
                    #print("num:", i, "prime:", p, "+ 2 x",  int(sqbase), "^2")
                    break
    if found == False:
        print ("no factors found for", i)
        quit()
    i += 2
