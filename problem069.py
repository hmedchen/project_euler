import numpy as np
import common_funcs as cf

# https://de.wikipedia.org/wiki/Eulersche_Phi-Funktion
limit = 1000000
prime_sieve = cf.prime_sieve(limit+1)
primes = cf.find_primes_below(limit+1)
rate = [[0,0.0,0.0]] * (limit+1)
rate[0]=[-1,-1,-1]
rate[1]=[1,1,1]
prime_factor_cache = [False] * (limit + 1)
filled = 2

val_buf = [0] + [1-(1/v) for v in range(1,limit + 1)]

print("Limit",limit)
print("calculated prime sieve")
test = [0,0,1,2,2,4,2,6,4,6,4]
loop_count = 0

def add_num(res,val):
    global filled
    global rate
    rate[res] = [val, val_buf[res], res/val]
    filled += 1

def verify():
    ver_val = [0]*100001
    correct = 0
    wrong = 0
    with open("problem069.verify.txt","r") as fp:
        lines = fp.readlines()
        for line in lines:
            if line.strip() != '':
                x,rho = [int(i) for i in line.strip().split()]
                ver_val[x] = rho
    for i in range(1,min(len(ver_val), limit)):
        if ver_val[i] != rate[i][0]:
            wrong += 1
            print("ERROR for value", i, ": should have",ver_val[i],", have",rate[i][0])
        else:
            correct += 1
    print("Correct:", correct, "wrong:", wrong)
    if wrong > 0:
        quit()

def prime_factors(x, primes = None, sieve = None):
    global prime_factor_cache
    sofar = 0
    remainder = x
    prime_factors = []
    max_prime = 0
    if x < 2:
        return []

    # init primes in case it's not alredy precomputed
    if primes is None:
        print("Calculate Primes...")
        primes = cf.find_primes_below(x+1)
    if sieve is None:
        print("Calculate Sieve...")
        sieve = cf.prime_sieve(x+1)

    for prime in primes:
        if prime < max_prime:
            continue
        if prime_factor_cache[remainder]:
            if remainder not in prime_factors:
                prime_factors += prime_factor_cache[remainder]
                prime_factors = list(set(prime_factors))
                max_prime = max(prime_factors)
            break
        elif sieve[remainder]:
            if remainder not in prime_factors:
                prime_factors.append(remainder)
            prime_factor_cache[x] = prime_factors
            return prime_factors
        elif remainder % prime == 0:
            if prime not in prime_factors:
                prime_factors.append(prime)
            remainder //= prime
            sofar = x - remainder
        if remainder < prime:
            break
    prime_factor_cache[x] = prime_factors
    return prime_factors


print ("calculating prime factors")
for i in range(2,limit+1):
    facts = prime_factors (i,primes,prime_sieve)
    phi = i
    for fact in facts:
        phi *= val_buf[fact]
    add_num(i,int(phi))

print ("Looking for max value")
maxval=0
maxind=0
cnt = 0
for n in range(len(rate)):
    if rate[n][2] > maxval:
        maxval = rate[n][2]
        maxind = n
    elif rate[n][2] == maxval:
        cnt += 1

print("Max index:", maxind, cnt)
