#The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
#Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
#(Please note that the palindromic number, in either base, may not include leading zeros.)

from common_funcs import is_palindromic

max_num=1000000

sum_num=0
for num in range(1,max_num):
  if is_palindromic(num):
    bin_num = int(bin(num)[2:])
    if is_palindromic(bin_num):
      sum_num+=num
      print (num, bin_num, sum_num)
