# A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation
#  of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically,
# we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
#
# 012   021   102   120   201   210
#
# What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?

import math

# frequency nth digit (from right) is factorial(n-1)

target = 1000000-1
members=sorted([0,1,2,3,4,5,6,7,8,9])
#target = 4-1
#members=[0,1,2]
x=[]
for i in range (len(members), 0, -1):
  fact = math.factorial(i-1)
  print "fact",  fact, " = ",
  pos =  target / math.factorial(i-1)
  print "pos",  pos
  x.append(members.pop(pos))
  target -= pos*fact
  print x, members

y = [str(i) for i in x]
print "".join(y)
