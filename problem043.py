# The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.
#
# Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:
#
# d2d3d4=406 is divisible by 2
# d3d4d5=063 is divisible by 3
# d4d5d6=635 is divisible by 5
# d5d6d7=357 is divisible by 7
# d6d7d8=572 is divisible by 11
# d7d8d9=728 is divisible by 13
# d8d9d10=289 is divisible by 17
# Find the sum of all 0 to 9 pandigital numbers with this property.

import common_funcs

sum = 0


def add_num(cand, allnums, primes):
    global sum
    if len(allnums) == 1:
        found = str(allnums[0]) + cand
        print("FOUND!", found)
        sum += int(found)
    else:
        for n in allnums:
            newcand = str(n) + cand
            if int(newcand[:3]) % primes[-1] == 0:
                tmpnums = allnums[:]
                tmpnums.remove(int(n))
                add_num(newcand, tmpnums, primes[:-1])


def eval_candidate(cand, primes):
    allnums = [n for n in range(0, 10)]
    for n in cand:
        if int(n) not in allnums:
            return False
        else:
            allnums.remove(int(n))
    add_num(cand, allnums, primes)


primes = common_funcs.find_primes_below(18)

for i in range(primes[-1], 988, primes[-1]):
    i = str(i).rjust(3, "0")
    eval_candidate(i, primes[:-1])
print(sum)
